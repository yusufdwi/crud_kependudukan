<?php 
// memanggil koneksi
	require_once('koneksi.php');
// untuk keamanan
  	require_once('satpamku.php');

	$query="SELECT * FROM tb_penduduk";
	$data=mysqli_query($conn,$query);

	// var_dump($data);
?>

<!DOCTYPE html>
<html>
 	<head>
		<title>Data Kependudukan</title>

	<!-- css -->
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<link rel="stylesheet" href="bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="style.css">
	<!-- css -->

 	</head>
<body>

	<!-- untuk menu atas -->
<div class="pos-f-t">
  <div class="collapse" id="navbarToggleExternalContent">
    <div class="bg-dark p-4">      
      <a href="logout.php" type="submit" class="btn btn-danger my-2 my-sm-0 btn-sm">Logout</a>
    </div>
  </div>
  <nav class="navbar navbar-light bg-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  </nav>
</div>

 <section class="bin">
		<h1 align="center">Data Kependudukan</h1>
		<hr>
		<br>

<table class="table table-bordered table-dark" align="center">
  <thead>
    <tr>
      <th scope="col">NIK</th>
      <th scope="col">NAMA</th>
      <th scope="col">GENDER</th>
      <th scope="col">ALAMAT</th>
      <th scope="col">STATUS</th>
      <th scope="col">GOLONGAN</th>
      <th scope="col">AKSI</th>
    </tr>
  </thead>

  <?php 
	   while ($row=mysqli_fetch_assoc($data)) {
  ?>

  <tbody>
    <tr>
      <td><?php echo $row ['nik']; ?></td>
  	  <td><?php echo $row ['nama']; ?></td>	
  	  <td><?php echo $row ['gender']; ?></td>
  	  <td><?php echo $row ['alamat']; ?></td>
  	  <td><?php echo $row ['status']; ?></td>
  	  <td><?php echo $row ['golongan']; ?></td>
	  <td>

      <a href="edit_data.php?nik=<?php echo $row ['nik']; ?>"><i class="material-icons" style="color: green">edit</i></a> |
	  	<a href="hapus_data.php?nik=<?php echo $row ['nik']; ?>" onclick="return confirm('Bener NIH...???')" ><i class="material-icons" style="color: red">delete_outline</i></a>
	  </td>

    </tr>
  </tbody>

 <?php 
	   }
 ?>

</table>
<div class="container">
<!-- ini button -->
    <a href="input_data.php" type="submit" class="btn btn-success my-2 my-sm-0 btn-sm"><i class="material-icons">add_box</i>Add</a>
<!-- ini button -->
</div>
</section>

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>