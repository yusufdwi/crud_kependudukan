<?php 
// untuk keamanan
  	require_once('satpamku.php');

	// var_dump($data);
?>

<section class="oke">
 	<div class="container">
	  		<div class="row">

	  		   <div class="col-sm-12" align="center">
	  			<h1>Input Data Kependudukan</h1>
	  			<hr>
	  			<br>
	  			<br>
	  		   </div>

            </div>
	</div>

	<!-- ini link -->
		<link rel="stylesheet" href="bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="style1.css">

	<section class="input">
   	<div class="container">  		
   		<div class="row">
   			<div class="col-sm-12">

   				<form method="POST" action="aksi_data.php">
	  	  			<div class="form-group">
	  	  				<h5 for="nik">NIK</h5>
	  	  				<input type="text" name="nik" class="form-control border-primary" placeholder="Masukan NIK" required="required">	  	  				
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="nama_nama">Nama</h5>
	  	  				<input type="text" name="nama" class="form-control" placeholder="Masukan Nama Anda">	  	  				
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="gender">Gender</h5>
	  	  				<p><input type="radio" name="gender" value="L">Laki-laki
	  	  				<input type="radio" name="gender" value="P">Perempuan</p>
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="alamat">Alamat</h5>
	  	  				<textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat Anda"></textarea>
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="status">Status</h5>
	  	  				<p><select class="form-control" name="status">
	  	  					<option>--PILIH--</option>
	  	  					<option >Nikah</option>
	  	  					<option>Belum Nikah</option>
	  	  				</select> 
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="golongan">Golongan</h5>
	  	  				<p><input type="radio" name="golongan" value="wni">WNI
	  	  				<input type="radio" name="golongan" value="wna">WNA</p>
	  	  			</div>

	  	  			
	  	  			<button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
	  	  			<button type="reset" name="batal" class="btn btn-danger">Batal</button>
	  	  		</form>
	  	</div>
	</div>

</section>