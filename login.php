<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<!-- untuk link -->
	<link rel="stylesheet" type="text/css" href="login.css">
	<link rel="stylesheet" href="bootstrap.min.css">
	<!-- untuk link -->
</head>
<body align="center">
	  <div class="container">
	  	<div class="login" style="background-color: #494b50">
	      <form method="POST" action="aksi_login.php">	
	      	<h4 align="center" style="color: white">Halaman Login</h4>
	      	<hr>
			<div class="username">
				<label style="color: white">Username</label>
				<input type="text" name="username" class="form-control ss" placeholder="Masukan Username">	  	  				
			</div>
			<br>
			<div class="password">
				<label style="color: white">Password</label>
				<input type="password" name="password" class="form-control ss" placeholder="Masukan Password">	  	  				
			</div>
			<br>
			<button type="submit" name="login" class="btn btn-primary">Login</button>
			 <button type="reset" name="batal" class="btn btn-danger">Batal</button>
	      </form>
	      </div>
	  </div>    
</body>
</html>