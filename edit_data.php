<?php
	
// untuk keamanan
  	require_once('satpamku.php');
// untuk komeksi	
	require_once('koneksi.php');
	$nik=$_GET['nik'];


	$query="SELECT * FROM tb_penduduk WHERE nik = '$nik'";
	$data=mysqli_query($conn,$query);

	$row=mysqli_fetch_assoc($data);

  ?>

<div class="container">
<div class="oke">

<!-- link css -->
 	<link rel="stylesheet" href="bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="style1.css">
 <!-- link css -->

  <h1 align="center">Ubah Data Kependudukan</h1>
  <hr>

<form method="POST" action="ubah_data.php">
	  	  			<div class="form-group">
	  	  				<h5 for="nik">NIK</h5>
	  	  				<input type="text" name="nik" class="form-control border-primary" placeholder="Masukan NIK" value="<?php echo $row ['nik']; ?>">	  	  				
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="nama_nama">Nama</h5>
	  	  				<input type="text" name="nama" class="form-control" placeholder="Masukan Nama Anda" value="<?php echo $row ['nama']; ?>">	  	  				
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="gender">Gender</h5>
	  	  				<p><input type="radio" name="gender" value="L" <?php if(!strcmp($row['gender'],"L")) { echo "CHECKED";} ?> >Laki-laki
	  	  				<input type="radio" name="gender" value="P" <?php if(!strcmp($row['gender'],"P")) { echo "CHECKED";} ?> >Perempuan</p>
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="alamat">Alamat</h5>
	  	  				<textarea class="form-control" name="alamat" rows="10" placeholder="Masukan Alamat Anda"><?php echo $row ['alamat']; ?></textarea>
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="status">Status</h5>
	  	  				<p><select class="form-control" name="status">
	  	  					<option>--PILIH--</option>
	  	  					<option value="nikah" <?php if(!strcmp($row['status'],"nikah")) { echo "SELECTED";} ?> >Nikah</option>
	  	  					<option value="belum_nikah" <?php if(!strcmp($row['status'],"belum_nikah")) { echo "SELECTED";} ?> >Belum Nikah</option>
	  	  				</select> 
	  	  			</div>

	  	  			<div class="form-group">
	  	  				<h5 for="golongan">Golongan</h5>
	  	  				<p><input type="radio" name="golongan" value="WNI"<?php if(!strcmp($row['golongan'],"WNI")) { echo "CHECKED";} ?> >WNI
	  	  				<input type="radio" name="golongan" value="WNA" <?php if(!strcmp($row['golongan'],"WNA")) { echo "CHECKED";} ?> >WNA</p>
	  	  			</div>

	  	  			
	  	  			<button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
	  	  			<button type="reset" name="batal" class="btn btn-danger">Batal</button>
	  	  		</form>
	</div>
</div>	


